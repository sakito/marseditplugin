#!/usr/bin/env php
<?php
require_once 'markdown.php';
require_once 'smartypants.php';

$text = file_get_contents('php://stdin', 'r');
$text = Markdown($text);
$text = SmartyPants($text);

echo $text;
?>