#!/usr/bin/env python

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
    from docutils.core import publish_parts
    from pygments_directive import *
except:
    pass

import sys
import codecs

rest = "".join(sys.stdin.readlines())
parts = publish_parts(source=rest, writer_name="html4css1")
print parts['fragment'].encode('utf-8')
