#!/usr/bin/env php
<?php
require_once 'OrgModeSyntax/OrgModeSyntax.php';

$text = file_get_contents('php://stdin', 'r');
$text = OrgModeSyntax::render($text);

echo $text;
?>